### functions and stuff for ploting games etc in the R-GUI

#' Show a position in an R plot
#' @param board A Laska position.
#' @param color Color to move in given position.
#' @return No return value.
plotBoard <- function( board, color) {
  if (color == white) titlestring <- "White to move" else titlestring <- "Black to move"
  op <- par(mai= c(0,0,0.2,0))
  xk<- c( rep(c(1, 3, 5, 7, 2, 4, 6)  , 3), c(1, 3, 5, 7))
  yk <- c(rep(1, 4), rep(2, 3), rep(3, 4), rep(4, 3), rep(5, 4), rep(6,3), rep(7,4))

  radius <- 0.7
  plot(c(4, 4),xlim=c(0,8), ylim=c(0,8), main=titlestring, xlab="", t="n",ylab="", axes=FALSE)

  symbols(xk, yk, circles=rep(radius, 25), add=TRUE, inch=FALSE)

  for (color in c(white, black)) {
      if (board[[towercounter.component]][color]){
          for (towerno in 1:board[[towercounter.component]][color]) {
###      cat(color)
#      if (tower[1] != 0) {
        plotTower( board[[color]][[towerno]] )
      }
    }
  }
  par(op)
}

#' Internal function: Plot tower in a laska board, called from plotBoard
#' @param tower A tower
#' @return No return value
plotTower <- function(tower) {
  centerpoints <- matrix( c( c( rep(c(1, 3, 5, 7, 2, 4, 6)  , 3), c(1, 3, 5, 7)),
                          c(rep(1, 4), rep(2, 3), rep(3, 4), rep(4, 3), rep(5, 4), rep(6,3), rep(7,4) ) ),
                         ncol=2, byrow=FALSE   )

  center <- centerpoints[abs(tower[1]),]
  
  stoneheight <- 2/11
  for (stone in 2:length(tower)) {
    plotStone( center -c(0, 0.5) + c(0, stoneheight*(stone-2) )  , tower[stone] )
  }
}

#' Internal function: Plot single stone as part of tower, calles from plotTower
#' @param center Coordinates of center of stone
#' @param stone Stone as integer
plotStone <- function(center, stone) {
  if (stone > 0) bgcolor <- "yellow" else bgcolor <- "lightblue"
  symbols(center[1], center[2], rectangles=matrix(c(0.6, 2/11), byrow=TRUE, ncol=2  ), add=TRUE, inch=FALSE, bg=bgcolor )
  if (abs(stone) == 2 ) text(center[1], center[2], "+")
}

readFieldFromPlot <- function(){
  centerpoints <- matrix( c( c( rep(c(1, 3, 5, 7, 2, 4, 6)  , 3), c(1, 3, 5, 7)),
                            c(rep(1, 4), rep(2, 3), rep(3, 4), rep(4, 3), rep(5, 4), rep(6,3), rep(7,4) ) ),
                         ncol=2, byrow=FALSE   )
  clicked <- locator(1)
  centerpoints[,1] <- centerpoints[,1] - clicked$x
  centerpoints[,2] <- centerpoints[,2] - clicked$y
  field <- which.min(apply(centerpoints * centerpoints, 1, sum))
  cat(field, "\n")
  return(field) 
}

readMoveFromPlot <- function( board, color){
  centerpoints <- matrix( c( c( rep(c(1, 3, 5, 7, 2, 4, 6)  , 3), c(1, 3, 5, 7)),
                            c(rep(1, 4), rep(2, 3), rep(3, 4), rep(4, 3), rep(5, 4), rep(6,3), rep(7,4) ) ),
                         ncol=2, byrow=FALSE   )

  foundMove <- FALSE

  candidates <- possibleJumps(board, color)
  if (length(candidates) > 0) {
    jumps.exist <- TRUE
    stepwidth <- 3
  } else {
    jumps.exist <- FALSE
    candidates <- possibleSteps(board, color)
    stepwidth <- 2
  }
  
  stillcandidate <- rep(TRUE, length(candidates))

###  print(candidates)
###  print(stillcandidate)
  
  currentfieldno <- 0
  
  while (! foundMove){
    currentfieldno <- currentfieldno + 1 
    field <- readFieldFromPlot()
###    print(field)
    possibleField <- FALSE

    for (candno in 1:length(candidates)) {
      if (stillcandidate[candno]) {
        move <- candidates[[candno]]
        if (currentfieldno %% 2 == 1) {
### startfield
          if (field == move[1+ (currentfieldno %/% 2) * stepwidth]) {
            possibleField <- TRUE
###            cat("Startfield")
          }
        }
        else {
          if (field == move[ (currentfieldno %/% 2) * stepwidth]) {
            possibleField <- TRUE
### targetfield
###            cat("Targetfield")
          }
        }
      }
    }

    if (possibleField) {
      for (candno in 1:length(candidates)) {
        if (stillcandidate[candno]) {
          move <- candidates[[candno]]
          if (currentfieldno %% 2 == 1) {
### starfield
            if (field != move[1+ (currentfieldno %/% 2) * stepwidth]) {
              stillcandidate[candno] <- FALSE
            }
          }
          else {
            if (field != move[ (currentfieldno %/% 2) * stepwidth]) {
              stillcandidate[candno] <- FALSE
### targetfield
            }
          }
###          cat("nach löschung\n")
###          print(move)
###          print(stillcandidate)
        }
      }
    } else {
      currentfieldno <- currentfieldno -1
    }
    if (sum(stillcandidate) == 1) {
      foundMove <- TRUE
    } else {
      symbols(centerpoints[field, 1], centerpoints[field, 2], circles=0.72, add=TRUE, inch=FALSE, col="red")
    }
  }
###  print(stillcandidate)
  return(candidates[[ which(stillcandidate)  ]])
}

